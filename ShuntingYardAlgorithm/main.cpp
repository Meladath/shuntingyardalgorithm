#include <stack>
#include <queue>
#include <string>
#include <iostream>

char GetOperatorPrecedence(char op)
{
  switch(op)
  {
  case '+': return 6; break;
  case '-': return 6; break;
  case '*': return 5; break;
  case '/': return 5; break;
  case '^': return 11; break;
  }
  return -1;
}

char GetOperatorAssociation(char op)
{
  auto opPrec = GetOperatorPrecedence(op);

  switch(opPrec)
  {
  case 1:
  case 2:
  case 4:
  case 5:
  case 6:
  case 7:
  case 8:
  case 9:
  case 10:
  case 11:
  case 12:
  case 13:
  case 14:
  case 17: return 1;
  case 3:
  case 15:
  case 16: return 0;
  }

  return -1;
}

void HandleOperator(char in, std::stack<char> &operatorStack, std::queue<int> &outputQueue)
{
  auto opAssoc = GetOperatorAssociation(in);
  auto opPrec = GetOperatorPrecedence(in);

  if(!operatorStack.empty())
  {
    auto precTopStack = GetOperatorPrecedence(operatorStack.top());

    while(!operatorStack.empty() && ((opAssoc == 1 && opPrec <= precTopStack) || (opAssoc == 0 && opPrec < precTopStack)))
    {
      outputQueue.push(operatorStack.top());
      operatorStack.pop();
    }
  }
  operatorStack.push(in);
}

//void HandleCloseParen(std::stack<char> &operatorStack, std::queue<int> &outputQueue)
//{
//  while(!operatorStack.empty())
//  {
//    while(!(operatorStack.top() == '('))
//    {
//      //TODO: Implement function tokens first
//    }
//  }
//}

int main()
{
  std::stack<char> operatorStack;
  std::queue<int> outputQueue;

  std::string input = "3+5*7/1+4";
  
  for(auto in : input)
  {
    switch(in)
    {
    case '+':
    case '-':
    case '*':
    case '/': HandleOperator(in, operatorStack, outputQueue); break;
    case '(': operatorStack.push(in);
    //case ')': HandleCloseParen(operatorStack, outputQueue);
    default: outputQueue.push(in); break;
    }
  }

  while(!operatorStack.empty())
  {
    outputQueue.push(operatorStack.top());
    operatorStack.pop();
  }

  while(!outputQueue.empty())
  {
    std::cout << (char)outputQueue.front() << std::endl;
    outputQueue.pop();
  }

  return 0;
}